//% block="16x2 LCD Display" color="#4d84ab"
namespace LCD1602 {
    // pin connections
    let dataPins = [
        DigitalPin.P8,
        DigitalPin.P12,
        DigitalPin.P2,
        DigitalPin.P3
    ];
    let registerPin=DigitalPin.P0;
    let enablePin=DigitalPin.P1;


    // commands
    let LCD_CLEARDISPLAY = 0x01;
    let LCD_RETURNHOME = 0x02;
    let LCD_ENTRYMODESET = 0x04;
    let LCD_DISPLAYCONTROL = 0x08;
    let LCD_CURSORSHIFT = 0x10;
    let LCD_FUNCTIONSET = 0x20;
    let LCD_SETCGRAMADDR = 0x40;
    let LCD_SETDDRAMADDR = 0x80;



    // flags for display entry mode
    let LCD_ENTRYRIGHT = 0x00;
    let LCD_ENTRYLEFT = 0x02;
    let LCD_ENTRYSHIFTINCREMENT = 0x01;
    let LCD_ENTRYSHIFTDECREMENT = 0x00;

    // flags for display on/off control
    let LCD_DISPLAYON = 0x04
    let LCD_DISPLAYOFF = 0x00
    let LCD_CURSORON = 0x02
    let LCD_CURSOROFF = 0x00
    let LCD_BLINKON = 0x01
    let LCD_BLINKOFF = 0x00

    // flags for function set
    let LCD_8BITMODE = 0x10
    let LCD_4BITMODE = 0x00
    let LCD_2LINE = 0x08
    let LCD_1LINE = 0x00
    let LCD_5x10DOTS = 0x04
    let LCD_5x8DOTS = 0x00



    //% block="initiate display"
    export function initDisplay() {
        // at least 50ms after power on
        basic.pause(50);
        // send rs, enable low - rw is tied to GND
        pins.digitalWritePin(registerPin, 0);
        pins.digitalWritePin(registerPin, 1);
        write4bits(0x03);
        basic.pause(5);
        write4bits(0x03);
        basic.pause(5);
        write4bits(0x03);
        basic.pause(2);
        write4bits(0x02);
        send(LCD_FUNCTIONSET | 0x08, 0);
        basic.pause(5);
        send(LCD_FUNCTIONSET | 0x08, 0);
        basic.pause(2);
        send(LCD_FUNCTIONSET | 0x08, 0);
        basic.pause(2);
        send(LCD_FUNCTIONSET | 0x08, 0);
        basic.pause(2);
        send(LCD_DISPLAYCONTROL | LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF, 0);
        clear()
        send(LCD_ENTRYMODESET | LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT, 0);
    }



    //% block="clear display"
    export function clear() {
        send(LCD_CLEARDISPLAY, 0);
        basic.pause(2)
    }

    //% block="reset cursor"
    export function home() {
        send(LCD_RETURNHOME, 0);
        basic.pause(2)
    }

    //% block="display text %text"
    export function showText(text:string) {
        for(let i = 0; i < text.length;i++) {
            send(text.charCodeAt(i), 1);
        }
    }


    function write4bits(value:number) {
        for(let i=0;i<4;i++) {
            pins.digitalWritePin(dataPins[i], ((value>>i) && 0x01))
        }
        pulseEnable()
    }
    // mid and low level commands
    function send(value:number,mode:number) {
        pins.digitalWritePin(registerPin, mode);
        write4bits(value);
        write4bits(value>>4);
    }
    function pulseEnable() {
        pins.digitalWritePin(enablePin, 0);
        basic.pause(1);
        pins.digitalWritePin(enablePin, 1);
        basic.pause(1);
        pins.digitalWritePin(enablePin, 0);
        basic.pause(1);
    }
    //% block="move cursor to x %col y %row"
    export function setCursor(col:number,row:number) {
        let orpart=col;
        if(row>0) {
            orpart = orpart + 0x40;
        }
        send(LCD_SETDDRAMADDR | orpart, 0)
    }
}