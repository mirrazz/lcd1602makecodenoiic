# LCD16x2 IIC-Free MakeCode Library

If you have a 16-by-2 LCD text display but no IIC/I2C adapter and you want to use with your microbit, this library is for you!

Based off of Python code in a Maker.Pro article, this allows you to hookup the Micro:bit the same exact way!

Port of demo in article: https://makecode.microbit.org/_HkqRLMT9vJzT
